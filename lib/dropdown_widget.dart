import 'package:flutter/material.dart';

class DropDownwidget extends StatefulWidget {
  DropDownwidget({Key? key}) : super(key: key);

  @override
  _DropDownwidgetState createState() => _DropDownwidgetState();
}

class _DropDownwidgetState extends State<DropDownwidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine'),
                Expanded(
                  child: Container(),
                ),
                DropdownButton(
                  items: [
                    DropdownMenuItem(
                      child: Text('-'),
                      value: '-',
                    ),
                    DropdownMenuItem(
                      child: Text('Pfizer'),
                      value: 'Pfizer',
                    ),
                    DropdownMenuItem(
                      child: Text('Johnson & Johnson'),
                      value: 'Johnson & Johnson',
                    ),
                    DropdownMenuItem(
                      child: Text('Sputnik V'),
                      value: 'Sputnik V',
                    ),
                    DropdownMenuItem(
                      child: Text('AstraZeneca'),
                      value: 'AstraZeneca',
                    ),
                    DropdownMenuItem(
                      child: Text('Novavax'),
                      value: 'Novavax',
                    ),
                    DropdownMenuItem(
                      child: Text('Sinopharm'),
                      value: 'Sinopharm',
                    ),
                    DropdownMenuItem(
                      child: Text('Sinovac'),
                      value: 'Sinovac',
                    ),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              vaccine,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
