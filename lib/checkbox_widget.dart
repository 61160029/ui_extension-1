import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
    padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
    child: Column(
      children: [
        Row(
          children: [
            Text(title),
            Expanded(
              child: Container(),
            ),
            Checkbox(value: value, onChanged: onChanged)
          ],
        ),
        Divider()
      ],
    ),
  );
}

class CheckBoxwidget extends StatefulWidget {
  CheckBoxwidget({Key? key}) : super(key: key);

  @override
  _CheckBoxwidgetState createState() => _CheckBoxwidgetState();
}

class _CheckBoxwidgetState extends State<CheckBoxwidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ComboBox'),
      ),
      body: ListView(
        children: [
          _checkBox('Check 1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('Check 2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('Check 3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
            child: Text('Save'),
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                      'check1: $check1, check2: $check2,check3: $check3')));
            },
          )
        ],
      ),
    );
  }
}
